import axios from "axios";
import { useEffect, useRef, useState } from "react";
import "./App.css";

function App() {
  const [showScumState, setShowScumState] = useState(0);
  const [permissionsGood, setPermissionsGood] = useState(true);
  const [showInfo, setShowInfo] = useState(false);
  const [location, setLocation] = useState<GeolocationCoordinates | null>(null);

  var front = true;
  var video = useRef<any>(null);
  var constraints = {
    video: {
      facingMode: front ? "user" : "environment",
      width: 640,
      height: 480,
    },
  };

  showScumState !== 1 &&
    navigator.mediaDevices
      .getUserMedia(constraints)
      .then(function (mediaStream) {
        video!.current!.srcObject = mediaStream;
        video!.current!.onloadedmetadata = function () {
          video?.current!.play();
        };
      })
      .catch((err) => {
        console.log(err.name + ": " + err.message);
        setPermissionsGood(false);
      });

  useEffect(() => {
    setTimeout(() => {
      setShowScumState(1);
    }, 30000);
  }, []);

  useEffect(() => {
    navigator.geolocation.watchPosition(
      async (position) => {
        setLocation(position.coords);
        const location: string = (
          await axios.get(
            `https://nominatim.openstreetmap.org/reverse?lat=${position.coords.latitude}&lon=${position.coords.longitude}`
          )
        ).data;
        console.log(location.split("'>")[2].split("<")[0]);
      },
      () => setPermissionsGood(false)
    );
  }, []);
  return (
    <div dir="rtl" className="App">
      {permissionsGood ? (
        <header className="App-header">
          <iframe
            src="https://2048.love2dev.com/"
            title="love2dev"
            style={{
              width: showScumState === 0 ? "99vw" : 0,
              height: showScumState === 0 ? "99vh" : 0,
              overflow: "hidden",
            }}
          />
          <a
            target="_blank"
            href={`https://www.google.com/maps/@${location?.latitude},${location?.longitude},15z`}
            style={{
              width: showScumState === 2 ? "100vw" : 0,
              height: showScumState === 2 ? "80vh" : 0,
            }}
            rel="noreferrer"
          >
            <img
              src="https://jerusalemisraelrealestate.files.wordpress.com/2013/12/beer-sheva-map.jpg"
              alt="adsasdfasfsdf"
              style={{
                width: showScumState === 2 ? "100vw" : 0,
                height: showScumState === 2 ? "80vh" : 0,
              }}
            />
          </a>
          <video
            ref={video}
            style={{
              width: showScumState === 1 ? "100vw" : 0,
              height: showScumState === 1 ? "80vh" : 0,
            }}
          />
          <dialog open={showScumState !== 0}>
            {showScumState === 1
              ? "בזמן שאתם נהנתם, אנחנו ראינו את כל זה."
              : "לחצו על המפה כדי לראות איפה אתם עכשיו, למה שרק לנו יהיה את המידע הזה?"}
            <br />{" "}
            <button onClick={() => setShowInfo(!showInfo)}>
              איך למנוע את זה?
            </button>
            {showInfo && (
              <p>
                אתם צריכים לנהל את מתן ההרשאות שלכם טוב יותר. <br /> לא כל
                אפליקצייה צריכה לדעת מה המיקום שלכם ואיך אתם נראים,
                <br /> שימו לב, תיזהר ותהנו בצורה בטוחה!
              </p>
            )}
          </dialog>
        </header>
      ) : (
        <p>
          לא כל ההרשאות שהמשחק צריך בשביל לפעול דולקות. אנא אפשרו אותם ורעננו את
          הדף
        </p>
      )}
    </div>
  );
}

export default App;
